
Installation
------------------------------

Uninstall node.js (optional)

Install node.js

Then run:

```shell
npm install
npm install -g gulp
```

or

```shell
npm install sharp && \
npm install gulp && \
npm install gulp-rename && \
npm install gulp-responsive && \
npm install speakingurl && \
npm install -g gulp
```

Edit file

```javascript
// \node_modules\gulp-responsive\lib\sharp.js

image.resize(width, height, {
  background: config.background,
  kernel: config.kernel,
  fit: config.fit, // <--- add this line
  withoutEnlargement: config.withoutEnlargement
})
```

Usage
------------------------------

Place your images to src folder, then run

```shell
gulp
```

You can also open bash on folder with images you need to crop, then run:

```shell
gulp fromFolder -f <path to this folder>/gulpfile.js
```

For example using Git Bash

```shell
gulp fromFolder -f /f/cropper/gulpfile.js
```
