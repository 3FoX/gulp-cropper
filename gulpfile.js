var gulp = require('gulp');
var responsive = require('gulp-responsive');
var rename = require("gulp-rename");
var translit = require('speakingurl');

gulp.task('default', gulp.series(convert));
gulp.task('fromFolder', gulp.series(fromFolder));

function convert()
{
    return gulp.src('src/**/*.{png,jpg,JPG,jpeg,JPEG,PNG}')
    .pipe(rename(function(path){
        path.basename = translit(path.basename)
    }))
    .pipe(responsive({
      '**/*.{png,jpg,JPG,jpeg,JPEG,PNG}': {
        width: 1000,
        height: 1000,
        format: 'jpg',
        rename: {
          suffix: '_900',
          // extname: '.jpg',
        },
      }
    }, {
      quality: 85,
      withoutEnlargement: false,
      fit: 'inside',
      sharpen: true,
      progressive: true,
    }))
    .pipe(gulp.dest('dist'));
}

function fromFolder()
{
    let width = 1920;
    let height = 1200;

    const source_path = process.env.PWD;
    const params = process.argv;

    for (let i = 0; i < params.length; i++) {
        if (params[i] === '-w' && params[i+1] !== undefined) {
            width = parseInt(params[i+1])
            height = parseInt(params[i+1])
        }
    }

    return gulp.src(source_path + '/**/*.{png,jpg,JPG,jpeg,JPEG,PNG}')
    .pipe(rename(function(path){
        path.basename = translit(path.basename)
    }))
    .pipe(responsive({
        '**/*.{png,jpg,JPG,jpeg,JPEG,PNG}': {
            width: width,
            height: height,
            format: 'jpg',
            rename: {
                suffix: '_' + width,
                // extname: '.jpg',
            },
        }
    }, {
        quality: 85,
        withoutEnlargement: false,
        fit: 'inside',
        sharpen: true,
        progressive: true,
        // errorOnUnusedImage: false,
        // errorOnEnlargement: false,
    }))
    .pipe(gulp.dest(source_path + '/dist'));
}

